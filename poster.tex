\documentclass{beamer}
\usepackage[orientation=portrait, size=a1, scale=1.4, debug]{beamerposter}
\usetheme{unn}

\usepackage{cmbright}

\usepackage{fontspec}
\setmainfont{CMU Sans Serif}
\setromanfont{CMU Sans Serif}
\setsansfont{CMU Sans Serif}

\usepackage{polyglossia}
\setmainlanguage{russian}

\usepackage{booktabs}
\usepackage{ragged2e}

\usepackage[backend=biber,
            movenames=false,
            maxnames=4,
            style=gost-numeric,
            sorting=nty,
            autolang=other]{biblatex}
\addbibresource{refs.bib}

\title{Исследование методов реализации неравномерной сетки для моделирования плазмы методом частиц в ячейках}
\author{А.В. Ларин\inst{1,2} \and Е.С. Ефименко\inst{1,2} \and А.А. Гоносков\inst{1,2,3}}
\institute{\inst{1}ННГУ им. Н.И. Лобачевского \and \inst{2}Институт прикладной физики РАН\\ \and \inst{3}Технологический университет Чалмерс}

\begin{document}
\begin{frame}[t]
    \begin{columns}[t]
        \begin{column}[t]{0.48\paperwidth}
            \begin{block}{Метод частиц в ячейках}
                \begin{itemize}
                    \item Один из распространённых подходов для моделирования плазмы.
                    \item Оперирует двумя сущностями: ансамбль макрочастиц и значения электромагнитного поля на сетке.
                    \item Частицы взаимодействуют друг с другом косвенно, генерируя токи, влияющие на динамику электромагнитного поля. Прямые взаимодейстия частиц не учитываются.
                \end{itemize}
                \begin{figure}
                    \includegraphics[width=0.9\textwidth]{img/pic}
                \end{figure}
                \vskip1ex
            \end{block}
            \vfill
            \begin{block}{Цель работы}
                Конечная цель --- реализация локального повышения разрешения в программном комплексе PICADOR, удовлетворяющая требованиям:
                \begin{itemize}
                    \justifying
                    \item Использование метода FDTD (finite-difference time-domain) интегрирования уравнений Максвелла.
                    \item Возможность обеспечить отражения от переходов между крупной (КС) и мелкой (МС) сетками с относительной амплитудой \(\leq10^{-6}\) для разрешений крупной сетки 16--40 ячеек на \(\lambda\).
                    \item Отсутствие или как можно более медленное развитие поздней неустойчивости.
                    \item Возможность интеграции схемы с операциями над частицами.
                \end{itemize}
            \end{block}
            \vfill
            \begin{block}{Особенности рассмотренных схем}
                \begin{itemize}
                    \justifying
                    \item Все сетки ограничены слоем поглощающих граничных условий PML (perfectly matched layers, красные ячейки на диаграммах ниже).
                    \item Для передачи полей между сетками используются мягкие источники в формулировке TFSF (total-field scattered-field, жирные зелёные стрелки на диаграммах ниже).
                    \item Могут использоваться вспомогательные сетки для обеспечения совпадения численной фазовой скорости передаваемых полей с таковой в целевой сетке, и уменьшения утечки в scattered-field область.
                \end{itemize}

                Примеры рассмотренных схем (крупная и мелкая сетка обозначены синим цветом, вспомогательные --- оранжевым):

                \begin{minipage}[t]{.48\textwidth}
                    \begin{figure}
                        \centering
                        \includegraphics[scale=1.9]{img/scheme1}
                        \caption{Базовая схема с мягкими источниками (S1).}
                        \label{fig:scheme1}
                    \end{figure}
                \end{minipage}
                \hfill
                \begin{minipage}[t]{.48\textwidth}
                    \begin{figure}
                        \centering
                        \includegraphics[scale=1.9]{img/scheme2}
                        \caption{Вспомогательная сетка разрывает петлю передач на мелкой сетке (S2).}
                        \label{fig:scheme2}
                    \end{figure}
                \end{minipage}
                \vskip6mm
                \begin{minipage}[t]{.66\textwidth}
                    \begin{figure}
                        \centering
                        \includegraphics[scale=1.9]{img/scheme3}
                        \caption{Схема с двумя вспомогательными сетками (S3).}
                        \label{fig:scheme-2auxgrids-swapsrc}
                    \end{figure}
                \end{minipage}
                \hfill
                \begin{minipage}[t]{.3\textwidth}
                    \begin{figure}
                        \centering
                        \includegraphics[scale=1.9]{img/sgdo}
                        \caption{Схема SG-DO, штриховые стрелки --- жёсткие источники.}
                    \end{figure}
                \end{minipage}
            \end{block}
            \vfill
        \end{column}
        \begin{column}[t]{0.48\paperwidth}
            \begin{block}{Результаты}
                Для схем S1--S3, их модификаций и SG-DO (Donderici, Teixeira, 2005) реализованы 1D прототипы, и они протестированы на задаче распространения TEM-импульса.
                \begin{itemize}
                    \justifying
                    \item Схемы S3 и SG-DO показывают достаточно низкие уровни отражений. Выбирая ширину областей PML и разрешение сеток, возможно обеспечить значения \(<10^{-6}\).
                        \vskip2ex
                        \begin{table}
                            \centering
                            \begin{tabular}{lrrrr}
                                \toprule
                                & \multicolumn{2}{c}{\(\lambda=8\Delta x_{KC}\)} & \multicolumn{2}{c}{\(\lambda=40\Delta x_{KC}\)}\\
                                \cmidrule(lr){2-3}
                                \cmidrule(lr){4-5}
                                Схема & КС \(\to\) МС & МС \(\to\) КС & КС \(\to\) МС & МС \(\to\) КС\\
                                \midrule
                                S1 & \(5.1\cdot10^{-3}\) & \(3.5\cdot10^{-2}\) & \(3.6\cdot10^{-5}\) & \(1.4\cdot10^{-3}\)\\
                                S2 & \(1.8\cdot10^{-6}\) & \(3.5\cdot10^{-2}\) & \(1.5\cdot10^{-6}\) & \(1.4\cdot10^{-3}\)\\
                                S3 & \(\mathbf{1.8\cdot10^{-6}}\) & \(\mathbf{3.1\cdot10^{-6}}\) & \(\mathbf{1.5\cdot10^{-6}}\) & \(\mathbf{2.6\cdot10^{-6}}\)\\
                                SG-DO & \(\mathbf{1.9\cdot10^{-6}}\) & \(\mathbf{2.1\cdot10^{-6}}\) & \(\mathbf{1.5\cdot10^{-6}}\) & \(\mathbf{3\cdot10^{-6}}\)\\
                                \bottomrule
                            \end{tabular}
                            \vskip1ex
                            \caption{Относительная амплитуда отражений от переходов между сетками (\(\frac{c\Delta t}{\Delta x_{MC}}=0.75\), \(\Delta x_{KC}=3\Delta x_{MC}\)).}
                        \end{table}
                    \item Рассмотренные схемы подвержены поздней неустойчивости из-за появления в мелкой сетке компоненты со значением \(k_x\), слишком большим для распространения в крупной сетке. В схеме SG-DO неустойчивость развивается медленнее, чем в остальных.
                        \begin{figure}
                            \centering
                            \includegraphics[width=0.9\textwidth]{img/spd-evolution}
                            \caption{Зависимость спектральной плотности мощности компоненты магнитного поля \(B_z\) на мелкой сетке от времени в S2. В левом нижнем углу --- распространение импульса, в правой части --- развитие поздней неустойчивости из компоненты с \(k_x\approx20\) см\({}^{-1}\).}
                            \label{fig:spd-evolution}
                        \end{figure}
                        На примере схемы S1 было установлено, что спектральная фильтрация полей при передаче на крупную сетку подавляет позднюю неустойчивость за счёт увеличения отражений.

                    \item Вычитание сгенерированных мягким источником полей при обратной передаче понижает отражения до приемлемого уровня, при этом ускоряя развитие неустойчивости.
                        \vskip2ex
                        \begin{table}
                            \centering
                            \begin{tabular}{lrrrr}
                                \toprule
                                & \multicolumn{2}{c}{\(\lambda=8\Delta x_{KC}\)} & \multicolumn{2}{c}{\(\lambda=40\Delta x_{KC}\)}\\
                                \cmidrule(lr){2-3}
                                \cmidrule(lr){4-5}
                                Схема & КС \(\to\) МС & МС \(\to\) КС & КС \(\to\) МС & МС \(\to\) КС\\
                                \midrule
                                S1 & \(5.1\cdot10^{-3}\) & \(3.5\cdot10^{-2}\) & \(3.6\cdot10^{-5}\) & \(1.4\cdot10^{-3}\)\\
                                S1 + вычитание & \(2.8\cdot10^{-6}\) & \(2.7\cdot10^{-6}\) & \(1.4\cdot10^{-6}\) & \(2.8\cdot10^{-6}\)\\
                                \bottomrule
                            \end{tabular}
                            \vskip1ex
                            \caption{Сравнение относительной амплитуды отражений в обычной S1 (см. рис.~\ref{fig:scheme1}) и в S1 с вычитанием при обратной передаче.}
                        \end{table}
                \end{itemize}
            \end{block}
            \vfill
            \begin{block}{Комплекс PICADOR}
                PICADOR --- высокопроизводительный программный комплекс для трёхмерного моделирования лазерно-плазменного взаимодействия.
                \begin{itemize}
                    \justifying
                    \item Ориентирован на гетерогенные системы с CPU и Xeon Phi.
                    \item Параллелизм организован с помощью MPI и OpenMP.
                    \item Имеет оптимизированное вычислительное ядро и расширяем.
                    \item Поддерживает разбиение расчётной области на прямоугольные домены с MPI-передачами только между соседними доменами и динамическую балансировку нагрузки.
                \end{itemize}
            \end{block}
            \vfill
        \end{column}
    \end{columns}
\end{frame}
\end{document}

