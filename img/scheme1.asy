import common;

pen thick = linewidth(2pt);

// coarse grid
fill((-.3,6)--(9,6)--(9,7)--(-.3,7)--cycle, unnblue1);
fill((9,6)--(10.3,6)--(10.3,7)--(9,7)--cycle, lightred);
draw((-.3,6)--(10.3,6), thick + unnblue4);
draw((10.3,7)--(-.3,7), thick + unnblue4);
for (int i = 0; i < 10; i += 3)
    draw((i,6)--(i,7), thick + unnblue4);

// fine grid
fill((2,0)--(12.3,0)--(12.3,1)--(2,1)--cycle, unnblue1);
fill((0.7,0)--(2,0)--(2,1)--(0.7,1)--cycle, lightred);
draw((0.7,0)--(12.3,0), thick + unnblue4);
draw((12.3,1)--(0.7,1), thick + unnblue4);
for (int i = 1; i < 13; i += 1)
    draw((i,0)--(i,1), thick + unnblue4);

// arrows
// fg2cg
fill((5.7,1.2)--(6.3,1.2)--(6.3,4.6)--(6.6,4.6)--(6,5.8)--(5.4,4.6)--(5.7,4.6)--cycle, green1);
draw((5.7,1.2)--(6.3,1.2)--(6.3,4.6)--(6.6,4.6)--(6,5.8)--(5.4,4.6)--(5.7,4.6)--cycle, thick + green4);
// cg2fg
fill((2.7,5.8)--(3.3,5.8)--(3.3,2.4)--(3.6,2.4)--(3,1.2)--(2.4,2.4)--(2.7,2.4)--cycle, green1);
draw((2.7,5.8)--(3.3,5.8)--(3.3,2.4)--(3.6,2.4)--(3,1.2)--(2.4,2.4)--(2.7,2.4)--cycle, thick + green4);

