import common;

pen thick = linewidth(2pt);
pen dashed = linetype(new real[] {3,3});

// coarse grid
fill((-1.3,9)--(6,9)--(6,10)--(-1.3,10)--cycle, unnblue1);
fill((6,9)--(7.3,9)--(7.3,10)--(6,10)--cycle, lightred);
draw((-1.3,9)--(7.3,9), thick + unnblue4);
draw((7.3,10)--(-1.3,10), thick + unnblue4);
for (int i = 0; i < 7; i += 3)
    draw((i,9)--(i,10), thick + unnblue4);

// fine grid
fill((2,0)--(7.3,0)--(7.3,1)--(2,1)--cycle, unnblue1);
fill((0.7,0)--(2,0)--(2,1)--(0.7,1)--cycle, lightred);
draw((0.7,0)--(7.3,0), thick + unnblue4);
draw((7.3,1)--(0.7,1), thick + unnblue4);
for (int i = 1; i < 8; i += 1)
    draw((i,0)--(i,1), thick + unnblue4);

// aux coarse grid
fill((0,6)--(3,6)--(3,7)--(0,7)--cycle, orange1);
fill((-1.3,6)--(0,6)--(0,7)--(-1.3,7)--cycle, lightred);
draw((-1.3,6)--(3,6)--(3,7)--(-1.3,7), thick + orange4);
draw((0,6)--(0,7), thick + orange4);

// aux fine grid
fill((3,3)--(4,3)--(4,4)--(3,4)--cycle, orange1);
fill((4,3)--(6.3,3)--(6.3,4)--(4,4)--cycle, lightred);
draw((6.3,3)--(3,3)--(3,4)--(6.3,4), thick + orange4);
for (int i = 4; i < 7; i += 1)
    draw((i,3)--(i,4), thick + orange4);


// arrows
// fg2acg
draw((3,1.2){up}..(2.3,3.5)..(3,5.8), arrow=Arrow(DefaultHead, 12bp), p=(thick + dashed + green4));
// acg2cg
fill((1.2,7.2)--(1.8,7.2)--(1.8,7.6)--(2.1,7.6)--(1.5,8.8)--(0.9,7.6)--(1.2,7.6)--cycle, green1);
draw((1.2,7.2)--(1.8,7.2)--(1.8,7.6)--(2.1,7.6)--(1.5,8.8)--(0.9,7.6)--(1.2,7.6)--cycle, thick + green4);
// cg2afg
draw((3,8.8){down}..(3.7,6.5)..(3,4.2), arrow=Arrow(DefaultHead, 12bp), p=(thick + dashed + green4));
// afg2fg
fill((3.2,2.8)--(3.8,2.8)--(3.8,2.4)--(4.1,2.4)--(3.5,1.2)--(2.9,2.4)--(3.2,2.4)--cycle, green1);
draw((3.2,2.8)--(3.8,2.8)--(3.8,2.4)--(4.1,2.4)--(3.5,1.2)--(2.9,2.4)--(3.2,2.4)--cycle, thick + green4);

